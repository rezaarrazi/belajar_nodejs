var express = require('express');
var router = express.Router();

var request = require('request');
var rows;

request('https://corona-virus-stats.herokuapp.com/api/v1/cases/countries-search', function (error, response, body) {
    if (!error && response.statusCode == 200) {
        var body_json = JSON.parse(body);
        rows = body_json['data']['rows'];

        // console.log(rows)
     }
})

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Covid 19 Monitoring', condition: true, array: rows });
});

module.exports = router;
